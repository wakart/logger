<?php

    namespace Wakart;

    use DateTime;
    use Psr\Log\AbstractLogger;
    use Psr\Log\InvalidArgumentException;
    use RuntimeException;
    use Stringable;

    class Logger extends AbstractLogger {

        protected string $file;

        /**
         * @throws \InvalidArgumentException
         * @throws \RuntimeException
         */
        public function __construct($file) {
            $this->initialize($file);
            $this->file = $file;
        }

        public function log($level, Stringable|string $message, array $context = []): void {
            $datetime = new DateTime();
            $datetime = $datetime->format(DATE_ATOM);

            $message = $this->interpolate($message, $context);

            $content = sprintf('[%s] %s: %s', $datetime, ucfirst($level), $message);
            $content .= ($context ? "\n" . print_r($context, true) : '');
            $content .= "\n";

            file_put_contents($this->file, $content, FILE_APPEND);
        }

        /**
         * @param string $file
         *
         * @throws \InvalidArgumentException
         * @throws \RuntimeException
         */
        protected function initialize(string $file): void {
            if (!$file) {
                throw new InvalidArgumentException('Log file is not specified.');
            }

            if (!file_exists($file) && !touch($file)) {
                throw new RuntimeException(sprintf('Log file %s can not be created.', $file));
            }

            if (!is_writable($file)) {
                throw new RuntimeException(sprintf('Log file %s is not writeable.', $file));
            }
        }

        private function interpolate($message, array $context = []): string {
            $replace = [];
            foreach ($context as $key => $val) {
                $replace['{' . $key . '}'] = $val;
            }
            return strtr($message, $replace);
        }
    }
